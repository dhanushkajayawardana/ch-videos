<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 8/7/19
 * Time: 12:56 PM
 */

namespace Creativehandles\ChVideos\Plugins\Videos;


use Creativehandles\ChVideos\Plugins\Plugin;
use Creativehandles\ChVideos\Plugins\Videos\Models\AttachmentModel;
use Creativehandles\ChVideos\Plugins\Videos\Repositories\AttachmentRepository;
use Creativehandles\ChVideos\Plugins\Videos\Repositories\VideoRepository;

class Videos extends Plugin
{
    /**
     * @var VideoRepository
     */
    private $videoRepository;
    /**
     * @var AttachmentRepository
     */
    private $attachmentRepository;

    public function __construct(VideoRepository $videoRepository, AttachmentRepository $attachmentRepository)
    {
        parent::__construct();
        $this->videoRepository = $videoRepository;
        $this->attachmentRepository = $attachmentRepository;
    }

    public function getAllVideos($perpage = 15, $with = ['attachments'])
    {
        return $this->videoRepository->allWithPagination($perpage, $with);
    }

    public function getAllVideosWithoutPagination()
    {
        return $this->videoRepository->allWithoutPagination();
    }

    public function createOrUpdateVideo($data, $id = 'id')
    {
        return $this->videoRepository->updateOrCreate($data, $id);
    }

    public function deleteVideoById($id)
    {
        return $this->videoRepository->deleteById($id);
    }

    public function getVideosByName($name)
    {
        return $this->videoRepository->findByLike('title', '%' . $name . '%');
    }

    public function getVideoAuthor()
    {
        return $this->videoRepository->getBy('trainings_instructor', Auth::user()->id);
    }

    public function findVideoById($id, $with = [])
    {
        return $this->videoRepository->find($id, $with);
    }
}