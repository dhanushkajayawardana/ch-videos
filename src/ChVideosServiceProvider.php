<?php

namespace Creativehandles\ChVideos;

use Creativehandles\ChVideos\Console\BuildVideosPackageCommand;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\ServiceProvider;

class ChVideosServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'ch-videos');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'ch-videos');
         $this->loadMigrationsFrom(__DIR__.'/../app/Plugins/Videos/Migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('ch-trainings.php'),
            ], 'config');

            $this->publishes([
                __DIR__.'/../app/Plugins/' => app_path('Plugins/'),
            ], 'pluginBase');

            // Publishing the blog controller.
            $this->publishes([
                __DIR__.'/../app/Http/Controllers/' => app_path('/Http/Controllers/'),
            ], 'pluginController');


            //publishing routes and breadcrumbs
            $this->publishes([
                __DIR__.'/../routes' => base_path('routes/packages'),
            ], 'routes');

            // Publishing the views.
            $this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/Admin/'),
            ], 'views');

            // Registering package commands.
            $this->commands([
                BuildVideosPackageCommand::class
            ]);
        }

    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'ch-videos');

        // Register the main class to use with the facade
        $this->app->singleton('ch-videos', function () {
            return new ChVideos;
        });
    }
}
