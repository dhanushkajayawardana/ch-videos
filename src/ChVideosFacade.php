<?php

namespace Creativehandles\ChVideos;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Creativehandles\ChVideos\Skeleton\SkeletonClass
 */
class ChVideosFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ch-videos';
    }
}
