# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/creativehandles/ch-videos.svg?style=flat-square)](https://packagist.org/packages/creativehandles/ch-videos)
[![Build Status](https://img.shields.io/travis/creativehandles/ch-videos/master.svg?style=flat-square)](https://travis-ci.org/creativehandles/ch-videos)
[![Quality Score](https://img.shields.io/scrutinizer/g/creativehandles/ch-videos.svg?style=flat-square)](https://scrutinizer-ci.com/g/creativehandles/ch-videos)
[![Total Downloads](https://img.shields.io/packagist/dt/creativehandles/ch-videos.svg?style=flat-square)](https://packagist.org/packages/creativehandles/ch-videos)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require creativehandles/ch-videos
```

## Usage

To activate Blog plugin with CORE CMS, you need to follow these steps.

1.Run `php artisan creativehandles:build-videos-plugin`

2.Finally run `php artisan cms:populate-permissions` command to populate permission tables.

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email deemantha@creativehandles.com instead of using the issue tracker.

## Credits

- [Deemantha Kasun](https://github.com/creativehandles)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).